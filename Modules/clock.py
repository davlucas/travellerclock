"""
Dislay the CLOCKS
"""
import tkinter as tk
import tkinter.messagebox as msg
import datetime
import pytz


class Clock:
    """
    For displaying the clocks
    """

    def __init__(self, **kwargs):
        """
        init variables for one Clock
        """
        self.dict_clock = dict()
        self.dict_clock["hourvar"] = tk.StringVar()
        self.dict_clock["t_z"] = kwargs.get("tz")
        self.dict_clock["place"] = kwargs.get("place")
        self.dict_clock["color"] = kwargs.get("color")
        self.root = kwargs.get("window")
        self.dict_clock["old_day"] = 32
        self.dict_clock["old_year"] = 100000
        self.init_display()
        self.update()

    def init_display(self):
        """
        create widgets
        """
        framebg = [self.dict_clock["color"][0], self.dict_clock["color"][1]]
        clockbg = [self.dict_clock["color"][2], self.dict_clock["color"][3]]
        col = self.dict_clock["place"][0]
        row = self.dict_clock["place"][1]
        self.dict_clock["frme"] = tk.Frame(self.root, bg=framebg[0], width=25,relief='groove',borderwidth=3)
        self.dict_clock["frme"].grid(row=row, column=col)
        self.dict_clock["label"] = tk.Label(
            self.dict_clock["frme"],
            bg=framebg[0],
            fg=framebg[1],
            font=("Helvetica", 14, "bold","underline"),
            width=15,
            relief="sunken",
            borderwidth=1,
            highlightcolor=framebg[0],
        )
        self.dict_clock["label"].grid(row=0, column=0)
        self.dict_clock["entry"]=tk.Label(
            self.dict_clock["frme"], bg=clockbg[0], fg=clockbg[1],
            textvariable=self.dict_clock["hourvar"],
            width=5,
            font=("Leslo LG L", 34),
            justify=tk.CENTER,
            relief='sunken',
            borderwidth=2
        )
        self.dict_clock["entry"].grid(row=1, column=0)
        self.dict_clock["labelday"] = tk.Label(
            self.dict_clock["frme"], bg=framebg[0], fg=framebg[1]
        )
        self.dict_clock["labelday"].config(
            width=11,
            height=2,
            bg=framebg[1],
            fg=framebg[0],
            font=("Sans", 10),
            relief="ridge",
            borderwidth=3,
        )
        self.dict_clock["labelday"].grid(row=2, column=0)

    def update(self):
        """
        update the widgets
        """
        self.timezone = pytz.timezone(self.dict_clock["t_z"])
        self.date = datetime.datetime.now(self.timezone)
        _table = self.dict_clock["t_z"].split("/")
        _city = _table[-1]
        minute = str(self.date.minute).zfill(2)
        day = str(self.date.day).zfill(2)
        if int(self.dict_clock["old_day"]) < int(day):
            msg.showinfo("info", "{} changed day".format(_city))
        year = str(self.date.year).zfill(2)
        if int(self.dict_clock["old_year"]) < int(year):
            msg.showinfo("info", "HAPPY NEW YEAR {}".format(_city))
        hour = str(self.date.hour).zfill(2)
        self.dict_clock["label"].config(text=_city)
        self.dict_clock["labelday"].config(
            text="{:%A}\n{:%d %b %Y}".format(self.date, self.date)
        )
        self.dict_clock["hourvar"].set("{}:{}".format(hour, minute))
        self.dict_clock["old_day"] = day
        self.dict_clock["old_year"] = year
        self.root.after(500, self.update)



    def remove(self):
        """Remove one Clock"""
        self.dict_clock["frme"].grid_forget()

class Local:
    def __init__(self,root):
        """
        Create
        litle
        local clock
        """
        self.root=root
        self.localtime = tk.StringVar()
        tk.Label(
            self.root,
            bg="black",
            fg="orange",
            textvariable=self.localtime,
            font=("Garuda", 10, "bold"),
            relief="raised",
            borderwidth=2,
        ).grid(column=0, row=0, columnspan=9,ipadx=30)
        self.update_localtime()
    def update_localtime(self):
        _time = datetime.datetime.now()
        self.localtime.set(
            "Local time:\t{}:{}:{} \t {:%A %d %B %Y}".format(
            str(_time.hour).zfill(2),
            str(_time.minute).zfill(2),
            str(_time.second).zfill(2),
            _time,
          )
         )
        self.root.after(500, self.update_localtime)
